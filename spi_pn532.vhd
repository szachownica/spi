--===========================================================================--
--                                                                           --
--             Synthesizable Serial Peripheral Interface Master              --
--                                                                           --
--===========================================================================--
--
--  File name      : spi-master.vhd
--
--  Entity name    : spi-master
--
--  Purpose        : Implements a SPI Master Controller
--
--  Dependencies   : ieee.std_logic_1164
--                   ieee.std_logic_unsigned
--
--  Author         : Hans Huebner
--
--  Email          : hans at the domain huebner.org
--
--  Web            : http://opencores.org/project,system09

--  Description    : This core implements a SPI master interface.
--                   Transfer size is 4, 8, 12 or 16 bits.
--                   The SPI clock is 0 when idle, sampled on
--                   the rising edge of the SPI clock.
--                   The SPI clock is derived from the bus clock input
--                   divided by 2, 4, 8 or 16.
--
--                   clk, reset, cs, rw, addr, data_in, data_out and irq
--                   represent the System09 bus interface.
--                   spi_clk, spi_mosi, spi_miso and spi_cs_n are the
--                   standard SPI signals meant to be routed off-chip.
--
--                   The SPI core provides for four register addresses
--                   that the CPU can read or writen to:
--
--                   Base + $00 -> DL: Data Low LSB
--                   Base + $01 -> DH: Data High MSB
--                   Base + $02 -> CS: Command/Status
--                   Base + $03 -> CO: Config
--
--                   CS: Write bits:
--
--                   CS[0]   START : Start transfer
--                   CS[1]   END   : Deselect device after transfer
--                                   (or immediately if START = '0')
--                   CS[2]   IRQEN : Generate IRQ at end of transfer
--                   CS[6:4] SPIAD : SPI device address
--
--                   CS: Read bits
--
--                   CS[0]   BUSY  : Currently transmitting data
--
--                   CO: Write bits
--
--                   CO[1:0] DIVIDE: SPI clock divisor,
--                                   00=clk/2,
--                                   01=clk/4,
--                                   10=clk/8,
--                                   11=clk/16
--                   CO[3:2] LENGTH: Transfer length,
--                                   00= 4 bits,
--                                   01= 8 bits,
--                                   10=12 bits,
--                                   11=16 bits
--
--  Copyright (C) 2009 - 2010 Hans Huebner
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
--
--===========================================================================--
--                                                                           --
--                              Revision  History                            --
--                                                                           --
--===========================================================================--
--
-- Version  Author        Date               Description
--
-- 0.1      Hans Huebner  23 February 2009   SPI bus master for System09
-- 0.2      John Kent     16 June 2010       Added GPL notice
--
--

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_unsigned.all;

--* @brief Synthesizable Serial Peripheral Interface Master
--*
--*
--* @author Hans Huebner and John E. Kent
--* @version 0.2 from 16 June 2010

entity spi_master_pn532 is
  port (
    clk      : in  std_logic;
    reset    : in  std_logic;
    start    : in  std_logic;
    data_in  : in  std_logic_vector(7 downto 0);
    data_out : out std_logic_vector(7 downto 0);
  	byte_end : out std_logic;
    --+ SPI Interface Signals
    spi_miso           : in  std_logic;
    spi_mosi           : out std_logic;
    spi_clk            : out std_logic;
    spi_cs_n           : out std_logic
    );
end;

--* @brief Implements a SPI Master Controller
--*
--* This core implements a SPI master interface.
--* Transfer size is 4, 8, 12 or 16 bits.
--* The SPI clock is 0 when idle, sampled on
--* the rising edge of the SPI clock.
--* The SPI clock is derived from the bus clock input
--* divided by 2, 4, 8 or 16.

architecture rtl of spi_master_pn532 is
  --* State type of the SPI transfer state machine
  type   state_type is (s_idle, s_running);
  signal state           : state_type;
  -- Shift register
  signal shift_reg       : std_logic_vector(7 downto 0) := (others => '0');
  -- Buffer to hold data to be sent
  signal spi_data_buf    : std_logic_vector(7 downto 0) := (others => '0');
  -- Number of bits transfered
  signal count           : std_logic_vector(2 downto 0) := (others => '0');
  -- Buffered SPI clock
  signal spi_clk_buf     : std_logic;
  -- Buffered SPI clock output
  signal spi_clk_out     : std_logic;
  -- Previous SPI clock state
  signal prev_spi_clk    : std_logic;
  -- Number of clk cycles-1 in this SPI clock period
  signal spi_clk_count   : std_logic_vector(3 downto 0) := X"0";
  -- SPI clock divisor
  signal spi_clk_divide  : std_logic_vector(1 downto 0) := "10";
  -- Internal chip select signal, will be demultiplexed through the cs_mux
  signal spi_cs          : std_logic;
  signal sent_buff_byte  :std_logic := '0';

begin

  spi_cs_n  <= not spi_cs;
  spi_clk   <= spi_clk_out;

read_data : process(clk,reset)
	begin
	 if reset = '1' then
      spi_data_buf    <= (others => '0');
    elsif falling_edge(clk) then
      spi_data_buf <= data_in;
    end if;
	end process;

read_out : process(clk, sent_buff_byte,shift_reg)
  begin
	if sent_buff_byte = '1' and sent_buff_byte'event then
		data_out <= shift_reg;
	end if;
  end process;

  --* SPI transfer state machine
spi_proc : process(clk, reset)
  begin
    if reset = '1' then
      count         <= (others => '0');
      shift_reg     <= (others => '0');
      prev_spi_clk  <= '0';
      spi_clk_out   <= '0';
      spi_cs        <= '0';
      state         <= s_idle;
	    sent_buff_byte <= '0';
    elsif falling_edge(clk) then
      prev_spi_clk <= spi_clk_buf;
      case state is
        when s_idle =>
          if start = '1' then
            count     <= (others => '0');
            shift_reg <= spi_data_buf;
            spi_cs    <= '1';
            sent_buff_byte <= '0';
            state     <= s_running;
          end if;
        when s_running =>
          if prev_spi_clk = '1' and spi_clk_buf = '0' then
            spi_clk_out <= '0';
            count       <= count + "001";
            shift_reg   <= shift_reg(6 downto 0) & spi_miso;
            if (count = "111") then
              state          <= s_idle;
              sent_buff_byte <= '1';
              byte_end       <= sent_buff_byte;
            end if;
          elsif prev_spi_clk = '0' and spi_clk_buf = '1' then
            spi_clk_out <= '1';
          end if;
        when others =>
          null;
      end case;
    end if;
  end process;

  spi_send_data : process(shift_reg)
  begin
      spi_mosi <= shift_reg(7);
  end process;



  --* Generate SPI clock
spi_clock_gen : process(clk, reset)
  begin
    if reset = '1' then
      spi_clk_count <= (others => '0');
      spi_clk_buf   <= '0';
    elsif falling_edge(clk) then
      if state = s_running then
        if  spi_clk_count = "1010" then -- 50Mhz -> 5Mhz
          spi_clk_buf   <= not spi_clk_buf;
          spi_clk_count <= (others => '0');
        else
          spi_clk_count <= spi_clk_count + "0001";
        end if;
      else
        spi_clk_buf <= '0';
      end if;
    end if;
  end process;
end rtl;
