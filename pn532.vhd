LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;

entity pn532 is
 port(
		clk         : in std_logic;
		reset       : in std_logic;
		tagUID      : out std_logic_vector(31 downto 0);
		tag_present : out std_logic
 );
end pn532;

architecture logic of pn532 is

constant read_tag_cmd_const          : std_logic_vector(31 downto 0) := X"D44A0100";
constant readtag_response_header     : std_logic_vector(15 downto 0) := X"D54B";
constant sam_configuration_cmd_const : std_logic_vector(39 downto 0) := X"D414011201";
constant sam_configuration_response  : std_logic_vector(15 downto 0) := X"D515";

type state_type is (s_sam_config_write, s_sam_config_ok, s_wait_for_tag, s_write_tag_cmd);
signal state : state_type := s_sam_config_write;

signal read_tag_cmd          : std_logic_vector(31 downto 0) := read_tag_cmd_const;
signal sam_configuration_cmd : std_logic_vector(39 downto 0) := sam_configuration_cmd_const;

signal start         : std_logic;
signal data_out      : std_logic_vector(7 downto 0) := X"00";
signal data_in       : std_logic_vector(7 downto 0):= X"00";
signal byte_end      : std_logic;
signal spi_miso      : std_logic;

--temp
signal send_bytes_counter : std_logic_vector(2 downto 0) := (others => '0');
signal data_buffer        : std_logic_vector(95 downto 0):= (others => '0') ;
signal tag_present_tmp    : std_logic:= '0';
signal timeout_counter    : std_logic_vector(23 downto 0) :=(others => '0');
signal configured         : std_logic:= '0';

begin
	tag_present <= tag_present_tmp;

	spi : entity work.spi_master_pn532(rtl)
		port map(
		clk      => clk,
		reset    => reset,
		start    => start,
		data_in  => data_in,
		data_out => data_out,
		byte_end => byte_end,
		spi_miso => spi_miso --"open" => as OPEN or no actual associated with it.
		);


write_data : process(clk, byte_end, state)
begin
if reset ='1' then
	start               <= '0';
	data_in             <= (others => '0');
  send_bytes_counter  <= (others => '0');
elsif rising_edge(byte_end) then
case state is
	 when s_write_tag_cmd =>
			data_in            <= read_tag_cmd(31 downto 24);
			read_tag_cmd       <= read_tag_cmd(23 downto 0) & read_tag_cmd(31 downto 24);
			send_bytes_counter <= send_bytes_counter +1;
			start              <= '1';
	when s_sam_config_write =>
		data_in               <= sam_configuration_cmd(39 downto 32);
		sam_configuration_cmd <= sam_configuration_cmd(31 downto 0) & sam_configuration_cmd(39 downto 32);
		send_bytes_counter    <= send_bytes_counter + 1;
		start                 <= '1';
  when s_sam_config_ok =>
    send_bytes_counter <= (others => '0');
  when s_wait_for_tag => --clk timeout
      if tag_present_tmp='1' then
        send_bytes_counter <= (others => '0');
      end if;
	when others =>
		null;
end case;
end if;
end process;

change_state : process(clk,reset, send_bytes_counter, configured)
begin
	if reset ='1' then
			sam_configuration_cmd <= sam_configuration_cmd_const;
			read_tag_cmd          <= read_tag_cmd_const;
			state                 <= s_sam_config_write;
	elsif rising_edge(clk) then
		case state is
			when s_sam_config_write =>
				if(send_bytes_counter = "101") then
						state            <= s_sam_config_ok;
					end if;
			when s_sam_config_ok =>
				if configured ='1' then
					state              <= s_write_tag_cmd;
		 		end if;
			when s_write_tag_cmd =>
 				if(send_bytes_counter = "011") then
 					state              <= s_wait_for_tag;
 				end if;
			when s_wait_for_tag => --clk timeout
				if tag_present_tmp='1' then
					state              <= s_write_tag_cmd;
				else --change to 50ms
					timeout_counter <= timeout_counter + 1;
					if(timeout_counter = X"2625A0") then
						timeout_counter <= (others => '0');
						state           <= s_write_tag_cmd;
					end if;
				end if;
			when others =>
				null;
		end case;
	end if;
end process;

read_data : process(clk,data_out)
begin
  if reset ='1' then
	   data_buffer           <= (others => '0');
     configured            <= '0';
  elsif rising_edge(clk) then
	data_buffer <= data_buffer(95 downto 8) & data_out;

	if(data_buffer(95 downto 80) = readtag_response_header) then
		tagUID <= data_buffer(31 downto 0);
		tag_present_tmp <= '1';
		data_buffer <= (others => '0');
	elsif data_buffer(95 downto 80) = sam_configuration_response then
		configured  <= '1';
		data_buffer <= (others => '0');
  else
    tag_present_tmp <= '0';
    configured      <= '0';
	end if;
end if;
end process;
end logic;
